type TaskProps = {
  id: string,
  title: string,
  description: string,
  date: Date,
  finished: boolean
};
