export default [
  {
    id: '1',
    title: 'Tarefa 1',
    description: 'Descrição da tarefa 1',
    date: new Date(2022, 1, 27, 15, 30, 0, 0),
    finished: false
  },
  {
    id: '2',
    title: 'Tarefa 2',
    description: 'Descrição teste teste  222',
    date: new Date(2022, 1, 27, 17, 10, 0, 0),
    finished: true
  }
];
