import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import Home from '../../pages/Home';

describe('Home page', () => {
  test('renders correctly', () => {
    render(<Home />);

    expect(
      screen.getByPlaceholderText('Filtrar tasks pela descrição')
    ).toBeInTheDocument();
    expect(screen.getByText('Ordernar por título')).toBeInTheDocument();
    expect(screen.getByText('Ordernar por data')).toBeInTheDocument();
    expect(screen.getByText('Adcionar task')).toBeInTheDocument();
  });

  test('should find task by description', async () => {
    render(<Home />);

    const searchTasksinput = screen.getByPlaceholderText(
      'Filtrar tasks pela descrição'
    );

    fireEvent.change(searchTasksinput, {
      target: { value: 'Descrição da tarefa 1' }
    });

    await waitFor(() => {
      expect(screen.getByText('Descrição da tarefa 1')).toBeInTheDocument();
    });

    await waitFor(() => {
      expect(
        screen.queryByText('Descrição teste teste  222')
      ).not.toBeInTheDocument();
    });
  });

  test('should open add task modal', () => {
    render(<Home />);

    const addTaskButton = screen.getByText('Adcionar task');

    fireEvent.click(addTaskButton);

    expect(screen.getByText('Adicionar task')).toBeInTheDocument();
    expect(screen.getByLabelText('Título')).toBeInTheDocument();
    expect(screen.getByLabelText('Descrição')).toBeInTheDocument();
    expect(screen.getByText('Adicionar')).toBeInTheDocument();
  });

  test('should open edit task modal', () => {
    render(<Home />);

    const editTaskButton = screen.getAllByText('Editar')[0];

    fireEvent.click(editTaskButton);

    expect(screen.getByText('Editar task')).toBeInTheDocument();
  });

  test('should remove task', async () => {
    render(<Home />);

    const deleteTaskButton = screen.getAllByText('Excluir')[0];

    fireEvent.click(deleteTaskButton);

    await waitFor(() => {
      expect(
        screen.queryByText('Descrição da tarefa 1')
      ).not.toBeInTheDocument();
    });
  });

  test('should order by title descending', async () => {
    render(<Home />);

    const orderTaskByTtileButton = screen.getByText('Ordernar por título');

    fireEvent.click(orderTaskByTtileButton);

    const firstCard = screen.getAllByTestId('taskCard')[0];

    await waitFor(() => {
      expect(firstCard).toHaveTextContent('Tarefa 2');
    });
  });

  test('should order by title crescent', async () => {
    render(<Home />);

    const orderTaskByTtileButton = screen.getByText('Ordernar por título');

    fireEvent.click(orderTaskByTtileButton);
    fireEvent.click(orderTaskByTtileButton);

    const firstCard = screen.getAllByTestId('taskCard')[0];

    await waitFor(() => {
      expect(firstCard).toHaveTextContent('Tarefa 1');
    });
  });

  test('should order by date descending', async () => {
    render(<Home />);

    const orderTaskByTtileButton = screen.getByText('Ordernar por data');

    fireEvent.click(orderTaskByTtileButton);

    const firstCard = screen.getAllByTestId('taskCard')[0];

    await waitFor(() => {
      expect(firstCard).toHaveTextContent('Tarefa 2');
    });
  });

  test('should order by date crescent', async () => {
    render(<Home />);

    const orderTaskByTtileButton = screen.getByText('Ordernar por data');

    fireEvent.click(orderTaskByTtileButton);
    fireEvent.click(orderTaskByTtileButton);

    const firstCard = screen.getAllByTestId('taskCard')[0];

    await waitFor(() => {
      expect(firstCard).toHaveTextContent('Tarefa 1');
    });
  });
});
