import SearchIcon from '@mui/icons-material/Search';
import { Button, TextField, InputAdornment } from '@mui/material';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import { useCallback, useEffect, useState } from 'react';
import AddTaskModal from '../components/AddTaskModal';
import EditTaskModal from '../components/EditTaskModal';
import Header from '../components/Header';
import TaskCard from '../components/TaskCard';
import mockedTasks from '../utils/tasksMock';

import styles from '../styles/app.module.scss';

const taskMock = mockedTasks;

type TitleOrder = 'descending' | 'crescent'
type DateOrder = 'descending' | 'crescent'

function App() {
  const [tasks, setTasks] = useState<TaskProps[]>(taskMock);
  const [filteredTasks, setFilteredTasks] = useState<TaskProps[]>(taskMock);
  const [titleOrder, setTitleOrder] = useState<TitleOrder>('crescent');
  const [dateOrder, setDateOrder] = useState<DateOrder>('crescent');

  const [descriptionSearch, setDescriptionSearch] = useState('');

  const [addTaskModalVisible, setAddTaskModalVisible] = useState(false);
  const [editTaskModalVisible, setEditTaskModalVisible] = useState(false);
  // eslint-disable-next-line prettier/prettier
  const [taskToEdit, setTaskToEdit] = useState<TaskProps>({} as TaskProps);

  const handleAddTask = useCallback((task: TaskProps) => {
    setTasks([...tasks, task]);
  }, [tasks]);

  const handleOpenEditTaskModal = useCallback((task: TaskProps) => {
    setEditTaskModalVisible(true);
    setTaskToEdit(task);
  }, [])

  const handleEditTask = useCallback((task: TaskProps) => {
    setTasks([...tasks].map(taskItem => taskItem.id === task.id ? task : taskItem));
  }, [tasks])

  const handleChangeFinishedStatus = useCallback((id: string) => {
    const findTask = tasks.find(task => task.id === id);
    if (findTask) {
      findTask.finished = !findTask.finished
      handleEditTask(findTask);
    }
  }, [handleEditTask, tasks])

  const handleRemoveTask = useCallback((id: string) => {
    setTasks([...tasks].filter(task => task.id !== id));
  },[tasks])

  function handleOrderByTitle() {
    switch (titleOrder) {
      case 'crescent':
        setTitleOrder('descending');
        break
      default:
        setTitleOrder('crescent');
    }
  }

  function handleOrderByDate() {
    switch (dateOrder) {
      case 'crescent':
        setDateOrder('descending');
        break
      default:
        setDateOrder('crescent');
    }
  }

  useEffect(() => {
    setFilteredTasks([...tasks].sort((a,b) => titleOrder === 'crescent' ?  a.title.localeCompare(b.title) :  b.title.localeCompare(a.title)))
  },[tasks, titleOrder])


  useEffect(() => {
    setFilteredTasks([...tasks].sort((a,b) => dateOrder === 'crescent' ?  a.date.getTime() - b.date.getTime() :  b.date.getTime() - a.date.getTime()))
  },[tasks, dateOrder])

  useEffect(() => {
    setFilteredTasks([...tasks].filter(task => task.description.toLowerCase().includes(descriptionSearch.toLowerCase())));
  },[descriptionSearch, tasks])

  return (
    <>
      <Header />
      <main>
        <div className={styles.inputButtonContainer}>
          <div>
            <TextField
              id="outlined-basic"
              placeholder="Filtrar tasks pela descrição"
              onChange={e => setDescriptionSearch(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                )
              }}
            />

            <Button
              variant="contained"
              endIcon={titleOrder === 'crescent' ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
              onClick={() => handleOrderByTitle()}
            >
              Ordernar por título
            </Button>

            <Button
              variant="contained"
              endIcon={dateOrder === 'crescent' ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
              onClick={() => handleOrderByDate()}
            >
              Ordernar por data
            </Button>
            <Button
              variant="contained"
              onClick={() => setAddTaskModalVisible(true)}
            >
              Adcionar task
            </Button>
          </div>
        </div>

        <AddTaskModal
          modalVisible={addTaskModalVisible}
          close={() => setAddTaskModalVisible(false)}
          addTask={task => handleAddTask(task)}
        />

        <EditTaskModal
          task={taskToEdit}
          modalVisible={editTaskModalVisible}
          close={() => setEditTaskModalVisible(false)}
          editTask={task => handleEditTask(task)}
        />

        <div className={styles.cards}>
          {filteredTasks.map(task => (
            <TaskCard
              key={task.id}
              task={task}
              onDeleteTask={id => handleRemoveTask(id)}
              onEditTask={taskSelected => handleOpenEditTaskModal(taskSelected)}
              onChangeFinishedStatus={id => handleChangeFinishedStatus(id)}
            />
          ))}
        </div>
      </main>
    </>
  );
}

export default App;
