import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import EditTaskModal from '.';
import tasksMock from '../../utils/tasksMock';

const mockedTask = tasksMock[0];
const mockCloseModal = jest.fn();
const mockEditTask = jest.fn();

describe('EditTaskModal component', () => {
  test('EditTaskModal renders correctly', () => {
    render(
      <EditTaskModal
        task={mockedTask}
        modalVisible
        close={mockCloseModal}
        editTask={mockEditTask}
      />
    );

    expect(screen.getByText('Editar task')).toBeInTheDocument();
    expect(screen.getByLabelText('Título')).toBeInTheDocument();
    expect(screen.getByLabelText('Descrição')).toBeInTheDocument();
    expect(screen.getByText('Editar')).toBeInTheDocument();
  });

  test('Should close modal', () => {
    render(
      <EditTaskModal
        task={mockedTask}
        modalVisible
        close={mockCloseModal}
        editTask={mockEditTask}
      />
    );

    const closeModalButton = screen.getByTestId('closeModal');

    fireEvent.click(closeModalButton);

    expect(mockCloseModal).toBeCalled();
  });

  test('Should display required field messages', async () => {
    render(
      <EditTaskModal
        task={mockedTask}
        modalVisible
        close={mockCloseModal}
        editTask={mockEditTask}
      />
    );

    const editTaskButton = screen.getByText('Editar');
    const titleInput = screen.getByLabelText('Título');
    const descriptionInput = screen.getByLabelText('Descrição');

    fireEvent.change(titleInput, { target: { value: '' } });
    fireEvent.change(descriptionInput, {
      target: { value: '' }
    });

    fireEvent.click(editTaskButton);

    await waitFor(() => {
      return expect(
        screen.getByText('Por favor insira o título da tarefa.')
      ).toBeInTheDocument();
    });

    await waitFor(() => {
      return expect(
        screen.getByText('Por favor insira uma descrição para a tarefa.')
      ).toBeInTheDocument();
    });
  });

  test('Should edit Task', async () => {
    render(
      <EditTaskModal
        task={mockedTask}
        modalVisible
        close={mockCloseModal}
        editTask={mockEditTask}
      />
    );

    const editTaskButton = screen.getByText('Editar');
    const titleInput = screen.getByLabelText('Título');
    const descriptionInput = screen.getByLabelText('Descrição');

    fireEvent.change(titleInput, { target: { value: 'Task editada' } });
    fireEvent.change(descriptionInput, {
      target: { value: 'Descrição da nova task editada.' }
    });

    fireEvent.click(editTaskButton);

    await waitFor(() => {
      return expect(mockEditTask).toBeCalledWith(
        expect.objectContaining({
          id: mockedTask.id,
          title: 'Task editada',
          description: 'Descrição da nova task editada.',
          date: mockedTask.date,
          finished: mockedTask.finished
        })
      );
    });
  });
});
