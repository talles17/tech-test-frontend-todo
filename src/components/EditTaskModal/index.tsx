import {
  Box,
  Modal,
  Typography,
  TextField,
  Button,
  IconButton,
  FormControl,
  FormHelperText
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import * as yup from 'yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { useEffect } from 'react';
import styles from '../AddTaskModal/addTaskModal.module.scss';

interface EditTaskModalProps {
  task: TaskProps;
  modalVisible: boolean;
  close: () => void;
  editTask: (task: TaskProps) => void;
}

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid #f26522',
  boxShadow: 24,
  p: 4
};

type EditTaskFormData = {
  title: string,
  description: string
};

const editTaskFormSchema = yup.object().shape({
  title: yup.string().required('Por favor insira o título da tarefa.'),
  description: yup
    .string()
    .required('Por favor insira uma descrição para a tarefa.')
});

export default function EditTaskModal({
  task,
  modalVisible,
  close,
  editTask
}: EditTaskModalProps) {
  const { register, handleSubmit, formState, reset, setValue } =
    useForm<EditTaskFormData>({
      resolver: yupResolver(editTaskFormSchema)
    });

  const { errors } = formState;

  function handleCloseModal() {
    reset();
    close();
  }

  const handleEditTask: SubmitHandler<EditTaskFormData> = async values => {
    const taskToUpdate = {
      ...task,
      ...values
    };
    editTask(taskToUpdate);
    handleCloseModal();
  };

  useEffect(() => {
    setValue('title', task.title);
    setValue('description', task.description);
  }, [setValue, task]);

  return (
    <Modal
      open={modalVisible}
      onClose={() => handleCloseModal()}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style} className={styles.container}>
        <form id="editTask" onSubmit={handleSubmit(handleEditTask)}>
          <div className={styles.modalHeader}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Editar task
            </Typography>
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="span"
              data-testid="closeModal"
              onClick={() => handleCloseModal()}
            >
              <CloseIcon color="disabled" />
            </IconButton>
          </div>
          <FormControl>
            <TextField
              id="outlined"
              label="Título"
              defaultValue={task.title}
              margin="normal"
              fullWidth
              error={!!errors.title}
              {...register('title')}
            />
            {errors.title && (
              <FormHelperText error id="my-helper-text">
                {errors.title.message}
              </FormHelperText>
            )}
          </FormControl>
          <TextField
            id="outlined-multiline-static"
            label="Descrição"
            defaultValue={task.description}
            multiline
            rows={4}
            margin="normal"
            fullWidth
            error={!!errors.description}
            {...register('description')}
          />

          {errors.description && (
            <FormHelperText error id="my-helper-text">
              {errors.description.message}
            </FormHelperText>
          )}
          <Button variant="contained" type="submit">
            Editar
          </Button>
        </form>
      </Box>
    </Modal>
  );
}
