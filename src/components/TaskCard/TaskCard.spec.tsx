import { fireEvent, render, screen } from '@testing-library/react';
import TaskCard from '.';

import mockedTasks from '../../utils/tasksMock';

const mockedTask = mockedTasks[0];

const onDeleteTaskMocked = jest.fn();
const onEditTaskMocked = jest.fn();
const onChangeFinishedStatusMocked = jest.fn();

describe('TaskCard component', () => {
  test('TaskCard renders correctly', () => {
    render(
      <TaskCard
        task={mockedTask}
        onDeleteTask={onDeleteTaskMocked}
        onEditTask={onEditTaskMocked}
        onChangeFinishedStatus={onChangeFinishedStatusMocked}
      />
    );

    expect(screen.getByText('Tarefa 1')).toBeInTheDocument();
    expect(screen.getByText('Descrição da tarefa 1')).toBeInTheDocument();
    expect(screen.getByText('27/02/2022 - 15:30')).toBeInTheDocument();
  });

  test('call delete task function', () => {
    render(
      <TaskCard
        task={mockedTask}
        onDeleteTask={onDeleteTaskMocked}
        onEditTask={onEditTaskMocked}
        onChangeFinishedStatus={onChangeFinishedStatusMocked}
      />
    );

    const deleteTaskButton = screen.getByText('Excluir');
    fireEvent.click(deleteTaskButton);

    expect(onDeleteTaskMocked).toHaveBeenCalledWith(mockedTask.id);
  });

  test('call edit task function', () => {
    render(
      <TaskCard
        task={mockedTask}
        onDeleteTask={onDeleteTaskMocked}
        onEditTask={onEditTaskMocked}
        onChangeFinishedStatus={onChangeFinishedStatusMocked}
      />
    );

    const editTaskButton = screen.getByText('Editar');
    fireEvent.click(editTaskButton);

    expect(onEditTaskMocked).toHaveBeenCalledWith(mockedTask);
  });

  test('call change task status function', () => {
    render(
      <TaskCard
        task={mockedTask}
        onDeleteTask={onDeleteTaskMocked}
        onEditTask={onEditTaskMocked}
        onChangeFinishedStatus={onChangeFinishedStatusMocked}
      />
    );

    const changeStatusTaskButton = screen.getByTestId('changeFinishedStatus');
    fireEvent.click(changeStatusTaskButton);

    expect(onChangeFinishedStatusMocked).toHaveBeenCalledWith(mockedTask.id);
  });
});
