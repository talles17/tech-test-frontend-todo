import {
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
  IconButton,
  Tooltip
} from '@mui/material';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import styles from './taskCard.module.scss';

interface TaskCardProps {
  task: TaskProps;
  onDeleteTask: (id: string) => void;
  onEditTask: (task: TaskProps) => void;
  onChangeFinishedStatus: (id: string) => void;
}

export default function TaskCard({
  task,
  onDeleteTask,
  onEditTask,
  onChangeFinishedStatus
}: TaskCardProps) {
  function onClickDeleteTask() {
    onDeleteTask(task.id);
  }

  function onClickEditTask() {
    onEditTask(task);
  }

  function onClickFinishedStatus() {
    onChangeFinishedStatus(task.id);
  }

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardContent>
        <div className={styles.cardHeader} data-testid="taskCard">
          <Typography gutterBottom variant="h5" component="div">
            {task.title}
          </Typography>
          <Tooltip
            title={task.finished ? 'Concluída' : 'Pendente'}
            placement="top"
          >
            <IconButton
              color="primary"
              aria-label="check"
              component="span"
              data-testid="changeFinishedStatus"
              onClick={() => onClickFinishedStatus()}
            >
              <CheckCircleIcon color={task.finished ? 'success' : 'disabled'} />
            </IconButton>
          </Tooltip>
        </div>
        <Typography variant="body2" color="text.secondary">
          {task.description}
        </Typography>
      </CardContent>
      <CardActions className={styles.cardActions}>
        <Typography variant="body2" color="text.secondary">
          {`${task.date.toLocaleDateString(
            'pt-BR'
          )} - ${task.date.getHours()}:${task.date.getMinutes()}`}
        </Typography>
        <div>
          <Button size="small" onClick={() => onClickEditTask()}>
            Editar
          </Button>
          <Button size="small" onClick={() => onClickDeleteTask()}>
            Excluir
          </Button>
        </div>
      </CardActions>
    </Card>
  );
}
