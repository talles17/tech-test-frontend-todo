import { render, screen } from '@testing-library/react';
import Header from '.';

describe('Header component', () => {
  test('Header renders correctly', () => {
    render(<Header />);

    expect(screen.getByText('Task App')).toBeInTheDocument();
  });
});
