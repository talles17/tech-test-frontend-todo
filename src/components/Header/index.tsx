import styles from './header.module.scss';

export default function Header() {
  return (
    <header className={styles.container}>
      <div>
        <h2>Task App</h2>
      </div>
    </header>
  );
}
