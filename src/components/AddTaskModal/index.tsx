import {
  Box,
  Modal,
  Typography,
  TextField,
  Button,
  IconButton,
  FormControl,
  FormHelperText
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { v4 as uuidv4 } from 'uuid';
import * as yup from 'yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import styles from './addTaskModal.module.scss';

interface AddTaskModalProps {
  modalVisible: boolean;
  close: () => void;
  addTask: (task: TaskProps) => void;
}

const style = {
  position: 'absolute',
  top: '30%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid #f26522',
  boxShadow: 24,
  p: 4
};

type AddTaskFormData = {
  title: string,
  description: string
};

const addTaskFormSchema = yup.object().shape({
  title: yup.string().required('Por favor insira o título da tarefa.'),
  description: yup
    .string()
    .required('Por favor insira uma descrição para a tarefa.')
});

export default function AddTaskModal({
  modalVisible,
  close,
  addTask
}: AddTaskModalProps) {
  const { register, handleSubmit, formState, reset } = useForm<AddTaskFormData>(
    {
      resolver: yupResolver(addTaskFormSchema)
    }
  );

  const { errors } = formState;

  function handleCloseModal() {
    reset();
    close();
  }

  const handleAddTask: SubmitHandler<AddTaskFormData> = values => {
    const task = {
      ...values,
      id: uuidv4(),
      date: new Date(),
      finished: false
    };
    addTask(task);
    handleCloseModal();
  };

  return (
    <Modal
      open={modalVisible}
      onClose={() => handleCloseModal()}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style} className={styles.container}>
        <form id="addTask" onSubmit={handleSubmit(handleAddTask)}>
          <div className={styles.modalHeader}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Adicionar task
            </Typography>
            <IconButton
              color="primary"
              aria-label="close modal"
              component="span"
              data-testid="closeModal"
              onClick={() => handleCloseModal()}
            >
              <CloseIcon color="disabled" />
            </IconButton>
          </div>
          <FormControl>
            <TextField
              id="outlined"
              label="Título"
              margin="normal"
              fullWidth
              error={!!errors.title}
              {...register('title')}
            />
            {errors.title && (
              <FormHelperText error id="my-helper-text">
                {errors.title.message}
              </FormHelperText>
            )}
          </FormControl>
          <TextField
            id="outlined-multiline-static"
            label="Descrição"
            multiline
            rows={4}
            margin="normal"
            fullWidth
            error={!!errors.description}
            {...register('description')}
          />

          {errors.description && (
            <FormHelperText error id="my-helper-text">
              {errors.description.message}
            </FormHelperText>
          )}
          <Button variant="contained" type="submit">
            Adicionar
          </Button>
        </form>
      </Box>
    </Modal>
  );
}
