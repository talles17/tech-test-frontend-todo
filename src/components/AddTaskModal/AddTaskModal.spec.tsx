import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import AddTaskModal from '.';

const mockCloseModal = jest.fn();
const mockAddTask = jest.fn();

describe('AddTaskModal component', () => {
  test('AddTaskModal renders correctly', () => {
    render(
      <AddTaskModal modalVisible close={mockCloseModal} addTask={mockAddTask} />
    );

    expect(screen.getByText('Adicionar task')).toBeInTheDocument();
    expect(screen.getByLabelText('Título')).toBeInTheDocument();
    expect(screen.getByLabelText('Descrição')).toBeInTheDocument();
    expect(screen.getByText('Adicionar')).toBeInTheDocument();
  });

  test('Should display required field messages', async () => {
    render(
      <AddTaskModal modalVisible close={mockCloseModal} addTask={mockAddTask} />
    );

    const addTaskButton = screen.getByText('Adicionar');
    fireEvent.click(addTaskButton);

    await waitFor(() => {
      return expect(
        screen.getByText('Por favor insira o título da tarefa.')
      ).toBeInTheDocument();
    });

    await waitFor(() => {
      return expect(
        screen.getByText('Por favor insira uma descrição para a tarefa.')
      ).toBeInTheDocument();
    });
  });

  test('Should close modal', () => {
    render(
      <AddTaskModal modalVisible close={mockCloseModal} addTask={mockAddTask} />
    );

    const closeModalButton = screen.getByTestId('closeModal');

    fireEvent.click(closeModalButton);

    expect(mockCloseModal).toBeCalled();
  });

  test('Should add new Task', async () => {
    render(
      <AddTaskModal modalVisible close={mockCloseModal} addTask={mockAddTask} />
    );

    const addTaskButton = screen.getByText('Adicionar');
    const titleInput = screen.getByLabelText('Título');
    const descriptionInput = screen.getByLabelText('Descrição');

    fireEvent.change(titleInput, { target: { value: 'Nova task' } });
    fireEvent.change(descriptionInput, {
      target: { value: 'Descrição da nova task.' }
    });

    fireEvent.click(addTaskButton);

    await waitFor(() => {
      return expect(mockAddTask).toBeCalledWith(
        expect.objectContaining({
          title: 'Nova task',
          description: 'Descrição da nova task.'
        })
      );
    });
  });
});
