# tech-test-frontend-todo

## Aplicação
O último deploy desta aplicação se encontra no endereço: https://infallible-mahavira-bfba02.netlify.app/

## Pré-requisitos
Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Yarn](https://classic.yarnpkg.com/pt-BR/).
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)



### 🛠 Tecnologias

Esse projeto foi feito de acordo com as instruções listadas, é utilizado o material UI como principal biblioteca de UI para ganhar tempo com implementações de components básicos, e possui testes unitarios de cada componente e página. O projeto é iniciado com duas tasks mockadas, e possui as funcionalidades de Adicionar task, editar task, excluir task, pesquisar task pela descrição, ordernar por título e ordernar por data de criação. Na minha opinião o melhor formato para exbição das tasks seria em uma Tabela, porém utilizei cards para exibição, pois ficava melhor para trabalhar a responsividade da página.

- [React](https://pt-br.reactjs.org/)
- [MUI](https://mui.com/pt/)
- [React Hook Form](https://react-hook-form.com/)
- [Yup](https://github.com/jquense/yup)
- [TypeScript](https://www.typescriptlang.org/)
- [Sass](https://sass-lang.com/)
- [esltint](https://eslint.org/)
- [prettier](https://prettier.io/)

### 🎲 Excutando a aplicação
```bash
# Clone este repositório
$ git clone https://gitlab.com/talles17/tech-test-frontend-todo

# Acesse a pasta do projeto no terminal/cmd
$ cd tech-test-frontend-todo

# Instale as dependências
$ yarn

# Execute a aplicação
$ yarn start

# A aplicação inciará na porta:3000 - acesse <http://localhost:3000>

# Para executar os testes
$ yarn test

# Para executar o test coverage
$ yarn test-coverage
```
